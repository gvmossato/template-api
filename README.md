# :computer: Exercício-Programa #1

Repositório destinado à entrega do primeiro exercício-programa da disciplina **MAC0475 - Laboratório de Sistemas Computacionais Complexos** com implementação das operação de criação e deleção de um usuário sob a perspectiva do *backend*.

## :memo: Tarefas

✔️ Implementação da ação de **criação** de usuário.

✔️ Implementação da ação de **deleção** de usuário.

## :pushpin: Dependências

* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)
* [Express](https://www.npmjs.com/package/express)
* [CORS](https://www.npmjs.com/package/cors)
* [UUID](https://www.npmjs.com/package/uuid)
* [JWT](https://www.npmjs.com/package/jsonwebtoken)
* [MongoDB](https://www.npmjs.com/package/mongodb)
* [NATS Streaming](https://www.npmjs.com/package/node-nats-streaming)
