const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  /* Initiate database and collection */

  const database = mongoClient.db('service')
  const users = database.collection('users')

  /* CREATE NEW USER */

  api.post("/users", (req, res) => {

    user = req.body;

    /* Test for missing fields */

    const fields = ["name", "email", "password", "passwordConfirmation"];

    for (const field of fields) {
      if (!user.hasOwnProperty(field)) {
        res.status(400).json({
          "error" : `Request body had missing field {${field}}`,
        })
      }
    }

    /* Test for malformed data */

    if (typeof user.name !== 'string' || user.name === '') {
      res.status(400).json({
        "error": "Request body had malformed field {name}",
      })
    }
    
    if (
      typeof user.email !== 'string' ||
      !user.email.match("^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$")
      ) {
        res.status(400).json({
          "error": "Request body had malformed field {email}",
        })
      }

    if (
      typeof user.password !== 'string' ||
      !user.password.match("^[A-Za-z0-9]+$") ||
      user.password.length < 8 ||
      user.password.length > 32
      ) {
        res.status(400).json({
          "error": "Request body had malformed field {password}",
        })
      }

    /* Test for wrong passwords */

    if (user.password !== user.passwordConfirmation) {
      res.status(422).json({
        "error": "Password confirmation did not match",
      })
    }

    /* Check if email is already registered */

    users.findOne({
      "user" : {
        "email" : user.email
      }
    })

    /* Register user */

    const id = uuid()

    res.status(201).json({
      "user" : {
        "id" : id,
        "name" : user.name,
        "email" : user.email
      }
    })

    /* Publish new user message */

    const C_msg = {
      "eventType": "UserCreated",
      "entityId": id,
      "entityAggregate": {
      "name": user.name,
      "email": user.email,
      "password": user.password,
      }
    }

    stanConn.publish('users', JSON.stringify(C_msg), (err, guid) => {
      if (err) {
        console.log(`Publish failed with error: ${err}`)
      } else {
        console.log(`Sucessfully published with guid: ${guid}`)
      }
    })    
  });

  /* DELETE OLD USER */

  api.delete("/users/:id", (req, res) => {

    head = req.headers;
    user_id = req.params.id; /* user to be removed */

    /* Test for auth token presence */

    if (!head.authentication) {
      res.status(401).json({
        "error" : "Access Token not found",
      })
    } else {
      token = head.authentication.split(" ")[1];
      issuer_id = jwt.decode(token).id /* user removing */
    }

    /* Test for request authorization */
    /* If user is trying to remove itself*/

    if (user_id !== issuer_id) {
      res.status(403).json({
        "error": "Access Token did not match User ID",
      })
    } else {

      /* Delete user */

      res.status(200).json({
        "id": user_id,
      })

      /* Publish deleted user message */

      const D_msg = {
        "eventType": "UserDeleted",
        "entityId": user_id,
        "entityAggregate": {}
      }

      stanConn.publish('users', JSON.stringify(D_msg), (err, guid) => {
        if (err) {
          console.log(`Publish failed with error: ${err}`)
        } else {
          console.log(`Sucessfully published with guid: ${guid}`)
        }
      })
      
    }

  });

  return api;
};
