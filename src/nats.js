const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    /* ******************* */

    /* Set options and subscribe to channel 'users' */

    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subs = conn.subscribe('users', opts);

    subs.on('message', (msg) => {
      console.log(`[${msg.getSequence()}] Received message: ${msg.getData()}`)
    })

  });

  return conn;
};
